<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostcodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('4pp_postcode', function($table) {
                $table->increments('id');
                
                $table->string('postcode', 7);
                $table->integer('postcode_id')->unsigned();
                $table->smallInteger('pnum')->unsigned();
                $table->char('pchar', 2);
                $table->mediumInteger('minnumber')->unsigned();
                $table->mediumInteger('maxnumber')->unsigned();
                $table->enum('numbertype', array('', 'mixed', 'even', 'odd'));
                $table->string('street', 100);
                $table->string('city', 100);
                $table->smallInteger('city_id')->unsigned()->nullable()->default(null);
                $table->string('municipality', 100)->nullable()->default(null);
                $table->smallInteger('municipality_id')->unsigned()->nullable()->default(null);
                $table->enum('province', array('','Drenthe','Flevoland','Friesland','Gelderland','Groningen','Limburg','Noord-Brabant','Noord-Holland','Overijssel','Utrecht','Zeeland','Zuid-Holland'))->nullable()->default(null);
                $table->enum('province_code', array('','DR','FL','FR','GE','GR','LI','NB','NH','OV','UT','ZE','ZH'))->nullable()->default(null);
                $table->decimal('lat', 15, 13)->nullable()->default(null);
                $table->decimal('lon', 15, 13)->nullable()->default(null);
                $table->decimal('rd_x', 31, 20)->nullable()->default(null);
                $table->decimal('rd_y', 31, 20)->nullable()->default(null);
                $table->enum('location_detail', array('','exact','postcode','pnum','city'))->default('postcode');
                $table->timestamp('changed_date');
                
                $table->index('postcode');
                $table->index(array('postcode', 'minnumber', 'maxnumber'));
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('4pp_postcode');
	}

}
