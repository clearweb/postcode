<?php

class PostcodeTableSeeder extends Seeder {
    public function run() {
        set_time_limit(0);
        ini_set('memory_limit', '512M');
        
        $sqlFilePath = storage_path() . '/postcode_NL.sql';
        $zipPath     = dirname(dirname(__FILE__)) . '/data/postcode_NL.sql.zip';
        
        if ( ! File::exists($sqlFilePath)) {
            $zip = new ZipArchive;
            $res = $zip->open($zipPath);
            if ($res === TRUE) {
                $zip->extractTo(storage_path());
                $zip->close();
            } else {
                throw new Exception('error extracting postcode zip');
            }
        }
        
        $rawQueries = File::get($sqlFilePath);
        $queries = explode('REPLACE INTO postcode ', $rawQueries);
        array_shift($queries);
        
        $queries = array_map(function($query) { return 'REPLACE INTO 4pp_postcode '.$query; }, $queries);
        
        foreach($queries as $query) {
            DB::unprepared($query);
        }
    }
}