<?php namespace Clearweb\Postcode;

use Illuminate\Support\ServiceProvider;

use Schema;

use PostcodeTableSeeder;

use Clearweb\Postcode\Postcode;

use Artisan;

class PostcodeServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        if ( ! Schema::hasTable('4pp_postcode')) {
            Artisan::call('migrate',['--package'=>'clearweb/postcode']);
        }
	}
    
    public function boot() {
        
        if (Postcode::count() < 471993) {
            $seeder = new PostcodeTableSeeder;
            $seeder->run();
        }
    }

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
